-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 19, 2018 at 09:44 PM
-- Server version: 5.6.38
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `rhsys`
--

-- --------------------------------------------------------

--
-- Table structure for table `Departamento`
--

CREATE TABLE `Departamento` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Departamento`
--

INSERT INTO `Departamento` (`id`, `nome`) VALUES
(2, 'teste1234');

-- --------------------------------------------------------

--
-- Table structure for table `Funcionario`
--

CREATE TABLE `Funcionario` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_bin NOT NULL,
  `sobrenome` varchar(255) COLLATE utf8_bin NOT NULL,
  `RG` varchar(15) COLLATE utf8_bin NOT NULL,
  `CPF` varchar(11) COLLATE utf8_bin NOT NULL,
  `telefone` varchar(11) COLLATE utf8_bin NOT NULL,
  `senha` varchar(255) COLLATE utf8_bin NOT NULL,
  `nivel` tinyint(1) NOT NULL,
  `cargo` enum('D','P','A','G','L') COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Funcionario`
--

INSERT INTO `Funcionario` (`id`, `nome`, `sobrenome`, `RG`, `CPF`, `telefone`, `senha`, `nivel`, `cargo`) VALUES
(1, 'haruan', 'justino', '86502259', '06872532920', '41996684423', '123456', 1, 'D');

-- --------------------------------------------------------

--
-- Table structure for table `FuncionarioDepartamento`
--

CREATE TABLE `FuncionarioDepartamento` (
  `departamento_id` int(11) NOT NULL,
  `funcionario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `FuncionarioSistema`
--

CREATE TABLE `FuncionarioSistema` (
  `sistema_id` int(11) NOT NULL,
  `funcionario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `FuncionarioSistema`
--

INSERT INTO `FuncionarioSistema` (`sistema_id`, `funcionario_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Salario`
--

CREATE TABLE `Salario` (
  `nivel` tinyint(1) NOT NULL,
  `cargo` enum('D','P','A','G','L') NOT NULL COMMENT '''Diretor'', ''Programador'', ''Analista'', ''Gerente'', ''Auxiliar de Limpeza''',
  `valor` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Salario`
--

INSERT INTO `Salario` (`nivel`, `cargo`, `valor`) VALUES
(1, 'D', '10000.00');
(1, 'P', '10000.00');
(1, 'A', '10000.00');
(1, 'G', '10000.00');
(2, 'D', '10000.00');
(2, 'P', '10000.00');
(2, 'A', '10000.00');
(2, 'G', '10000.00');
(3, 'D', '10000.00');
(3, 'P', '10000.00');
(3, 'A', '10000.00');
(3, 'G', '10000.00');
(1, 'L', '10000.00');

-- --------------------------------------------------------

--
-- Table structure for table `Sistema`
--

CREATE TABLE `Sistema` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Sistema`
--

INSERT INTO `Sistema` (`id`, `nome`) VALUES
(2, 'Administração'),
(1, 'RH');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Departamento`
--
ALTER TABLE `Departamento`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nome_UNIQUE` (`nome`);

--
-- Indexes for table `Funcionario`
--
ALTER TABLE `Funcionario`
  ADD PRIMARY KEY (`id`,`nivel`,`cargo`),
  ADD UNIQUE KEY `CPF_UNIQUE` (`CPF`),
  ADD KEY `fk_Funcionario_Salario1_idx` (`nivel`,`cargo`);

--
-- Indexes for table `FuncionarioDepartamento`
--
ALTER TABLE `FuncionarioDepartamento`
  ADD PRIMARY KEY (`departamento_id`,`funcionario_id`),
  ADD KEY `fk_Departamento_has_Funcionario_Funcionario1_idx` (`funcionario_id`),
  ADD KEY `fk_Departamento_has_Funcionario_Departamento1_idx` (`departamento_id`);

--
-- Indexes for table `FuncionarioSistema`
--
ALTER TABLE `FuncionarioSistema`
  ADD PRIMARY KEY (`sistema_id`,`funcionario_id`),
  ADD KEY `fk_Sistema_has_Funcionario_Funcionario1_idx` (`funcionario_id`),
  ADD KEY `fk_Sistema_has_Funcionario_Sistema_idx` (`sistema_id`);

--
-- Indexes for table `Salario`
--
ALTER TABLE `Salario`
  ADD PRIMARY KEY (`nivel`,`cargo`);

--
-- Indexes for table `Sistema`
--
ALTER TABLE `Sistema`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nome_UNIQUE` (`nome`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Departamento`
--
ALTER TABLE `Departamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Funcionario`
--
ALTER TABLE `Funcionario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Sistema`
--
ALTER TABLE `Sistema`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Funcionario`
--
ALTER TABLE `Funcionario`
  ADD CONSTRAINT `fk_Funcionario_Salario1` FOREIGN KEY (`nivel`,`cargo`) REFERENCES `Salario` (`nivel`, `cargo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `FuncionarioDepartamento`
--
ALTER TABLE `FuncionarioDepartamento`
  ADD CONSTRAINT `fk_Departamento_has_Funcionario_Departamento1` FOREIGN KEY (`departamento_id`) REFERENCES `Departamento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Departamento_has_Funcionario_Funcionario1` FOREIGN KEY (`funcionario_id`) REFERENCES `Funcionario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `FuncionarioSistema`
--
ALTER TABLE `FuncionarioSistema`
  ADD CONSTRAINT `fk_Sistema_has_Funcionario_Funcionario1` FOREIGN KEY (`funcionario_id`) REFERENCES `Funcionario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Sistema_has_Funcionario_Sistema` FOREIGN KEY (`sistema_id`) REFERENCES `Sistema` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

