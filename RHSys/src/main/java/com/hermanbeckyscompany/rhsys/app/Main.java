/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hermanbeckyscompany.rhsys.app;
import com.hermanbeckyscompany.rhsys.dao.ConnectionManager;
import com.hermanbeckyscompany.rhsys.dao.PersistenceException;
import com.hermanbeckyscompany.rhsys.ui.HomeFrame;
import com.hermanbeckyscompany.rhsys.ui.LoginFrame;

/**
 *
 * @author haruan
 */
public class Main {

   public static void main(String[] args) {
       LoginFrame loginFrame = new LoginFrame();
       loginFrame.setVisible(true);
    }
   
   public static void showHome() {
       HomeFrame homeFrame = new HomeFrame();
       homeFrame.setVisible(true);
   }
}
