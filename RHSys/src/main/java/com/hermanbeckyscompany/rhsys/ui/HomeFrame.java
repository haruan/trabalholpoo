/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hermanbeckyscompany.rhsys.ui;

/**
 *
 * @author luann
 */
public class HomeFrame extends javax.swing.JFrame {

    /**
     * Creates new form HomeFrame
     */
    public HomeFrame() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jRadioButtonMenuItem1 = new javax.swing.JRadioButtonMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jRadioButtonMenuItem2 = new javax.swing.JRadioButtonMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        departamentosButton = new javax.swing.JButton();
        sistemasButton = new javax.swing.JButton();
        funcionariosButton = new javax.swing.JButton();
        cargosButton = new javax.swing.JButton();

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        jRadioButtonMenuItem1.setSelected(true);
        jRadioButtonMenuItem1.setText("jRadioButtonMenuItem1");

        jMenu1.setText("jMenu1");

        jRadioButtonMenuItem2.setSelected(true);
        jRadioButtonMenuItem2.setText("jRadioButtonMenuItem2");

        jMenuItem1.setText("jMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(102, 102, 102));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setMaximumSize(new java.awt.Dimension(0, 0));
        setMinimumSize(null);
        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(800, 600));
        setResizable(false);
        setSize(new java.awt.Dimension(800, 600));

        departamentosButton.setBackground(new java.awt.Color(0, 204, 204));
        departamentosButton.setText("Departamentos");
        departamentosButton.setAlignmentY(0.0F);
        departamentosButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        departamentosButton.setHideActionText(true);
        departamentosButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        departamentosButton.setMaximumSize(new java.awt.Dimension(300, 100));
        departamentosButton.setMinimumSize(null);
        departamentosButton.setName(""); // NOI18N
        departamentosButton.setPreferredSize(null);
        departamentosButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                departamentosButtonActionPerformed(evt);
            }
        });

        sistemasButton.setBackground(new java.awt.Color(0, 204, 204));
        sistemasButton.setText("Sistemas");
        sistemasButton.setAlignmentY(0.0F);
        sistemasButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        sistemasButton.setHideActionText(true);
        sistemasButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        sistemasButton.setMaximumSize(new java.awt.Dimension(300, 100));
        sistemasButton.setMinimumSize(null);
        sistemasButton.setName(""); // NOI18N
        sistemasButton.setPreferredSize(null);
        sistemasButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sistemasButtonActionPerformed(evt);
            }
        });

        funcionariosButton.setBackground(new java.awt.Color(0, 204, 204));
        funcionariosButton.setText("Funcionários");
        funcionariosButton.setAlignmentY(0.0F);
        funcionariosButton.setHideActionText(true);
        funcionariosButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        funcionariosButton.setMaximumSize(new java.awt.Dimension(300, 100));
        funcionariosButton.setMinimumSize(null);
        funcionariosButton.setName(""); // NOI18N
        funcionariosButton.setPreferredSize(null);
        funcionariosButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                funcionariosButtonActionPerformed(evt);
            }
        });

        cargosButton.setBackground(new java.awt.Color(0, 204, 204));
        cargosButton.setText("Cargos");
        cargosButton.setAlignmentY(0.0F);
        cargosButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        cargosButton.setHideActionText(true);
        cargosButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        cargosButton.setMaximumSize(new java.awt.Dimension(300, 100));
        cargosButton.setMinimumSize(new java.awt.Dimension(300, 100));
        cargosButton.setName(""); // NOI18N
        cargosButton.setPreferredSize(null);
        cargosButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cargosButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(250, 250, 250)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cargosButton, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(departamentosButton, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sistemasButton, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(funcionariosButton, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(250, 250, 250))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(62, 62, 62)
                .addComponent(sistemasButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(departamentosButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(funcionariosButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cargosButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(112, 112, 112))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void departamentosButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_departamentosButtonActionPerformed
        DepartamentosFrame frame = new DepartamentosFrame();
        frame.setHomeFrame(this);
        frame.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_departamentosButtonActionPerformed

    private void sistemasButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sistemasButtonActionPerformed
        SistemasFrame frame = new SistemasFrame();
        frame.setHomeFrame(this);
        frame.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_sistemasButtonActionPerformed

    private void funcionariosButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_funcionariosButtonActionPerformed
        FuncionariosFrame frame = new FuncionariosFrame();
        frame.setHomeFrame(this);
        frame.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_funcionariosButtonActionPerformed

    private void cargosButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cargosButtonActionPerformed
        SalariosFrame frame = new SalariosFrame();
        frame.setHomeFrame(this);
        frame.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_cargosButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HomeFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HomeFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HomeFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HomeFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new HomeFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cargosButton;
    private javax.swing.JButton departamentosButton;
    private javax.swing.JButton funcionariosButton;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem1;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem2;
    private javax.swing.JButton sistemasButton;
    // End of variables declaration//GEN-END:variables
}
