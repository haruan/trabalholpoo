/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hermanbeckyscompany.rhsys.dao;

/**
 *
 * @author haruan
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

/**
 * Componente responsável por abrir e encerrar a conexão com o banco de dados.
 *
 * @author YaW Tecnologia
 */
public class ConnectionManager {

    //Informacões para conexão com banco de dados MYSQL.
    private static final String STR_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DATABASE = "rhsys";
    private static final String USER = "root";
    private static final String PASSWORD = "root";
    private static final String STR_CON = "jdbc:mysql://localhost/" + DATABASE + "?user=" + USER + "&password=" + PASSWORD;

    public static Connection getConnection() throws PersistenceException {
        Connection conn = null;
        try {
            Class.forName(STR_DRIVER);
            conn = DriverManager.getConnection(STR_CON);
            conn.setAutoCommit(false);

            System.out.println("Aberta a conexão com banco de dados!");
            return conn;
        } catch (ClassNotFoundException e) {
            String errorMsg = "Driver (JDBC) não encontrado";
            throw new PersistenceException(errorMsg, e);
        } catch (SQLException e) {
            String errorMsg = "Erro ao obter a conexão";
            throw new PersistenceException(errorMsg, e);
        }
    }

    public static void closeAll(Connection conn) {
        try {
            if (conn != null) {
                conn.close();
                System.out.println("Fechada a conexão com banco de dados!");
            }
        } catch (Exception e) {
            System.out.println("Não foi possivel fechar a conexão com o banco de dados!");
        }
    }

    public static void closeAll(Connection conn, Statement stmt) {
        try {
            if (conn != null) {
                closeAll(conn);
            }
            if (stmt != null) {
                stmt.close();
            }
        } catch (Exception e) {
            System.out.println("Não foi possivel fechar o statement!");
        }
    }

    public static void closeAll(Connection conn, Statement stmt, ResultSet rs) {
        try {
            if (conn != null || stmt != null) {
                closeAll(conn, stmt);
            }
            if (rs != null) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println("Não foi possivel fechar o resultSet!");
        }
    }

}
