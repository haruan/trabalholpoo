/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hermanbeckyscompany.rhsys.dao;

/**
 *
 * @author haruan
 */
import com.hermanbeckyscompany.rhsys.model.Funcionario;
import com.hermanbeckyscompany.rhsys.model.Sistema;
import java.sql.*;
import java.util.*;

public class FuncionarioDao {

    public Funcionario createValueObject() {
          return new Funcionario();
    }

    public Funcionario getObject(Connection conn, int id) throws NotFoundException, SQLException {

          Funcionario valueObject = createValueObject();
          valueObject.setId(id);
          load(conn, valueObject);
          return valueObject;
    }

    public void login(Connection conn, Funcionario funcionario, Sistema sistema) throws NotFoundException, SQLException {

          String sql = "SELECT * FROM Funcionario INNER JOIN FuncionarioSistema ON FuncionarioSistema.funcionario_id = funcionario.id WHERE (cpf = ? AND senha = ?  AND sistema_id = ?)"; 
          PreparedStatement stmt = null;

          try {
               stmt = conn.prepareStatement(sql);
               stmt.setString(1, funcionario.getCPF()); 
               stmt.setString(2, funcionario.getSenha()); 
               stmt.setInt(3, sistema.getId()); 

               singleQuery(conn, stmt, funcionario);

          } finally {
              if (stmt != null)
                  stmt.close();
          }
    }
        
    public void load(Connection conn, Funcionario valueObject) throws NotFoundException, SQLException {

          String sql = "SELECT * FROM Funcionario WHERE (id = ? ) "; 
          PreparedStatement stmt = null;

          try {
               stmt = conn.prepareStatement(sql);
               stmt.setInt(1, valueObject.getId()); 

               singleQuery(conn, stmt, valueObject);

          } finally {
              if (stmt != null)
                  stmt.close();
          }
    }


    /**
     * LoadAll-method. This will read all contents from database table and
     * build a List containing valueObjects. Please note, that this method
     * will consume huge amounts of resources if table has lot's of rows. 
     * This should only be used when target tables have only small amounts
     * of data.
     *
     * @param conn         This method requires working database connection.
     */
    public List loadAll(Connection conn) throws SQLException {

          String sql = "SELECT * FROM Funcionario ORDER BY id ASC ";
          List searchResults = listQuery(conn, conn.prepareStatement(sql));

          return searchResults;
    }



    /**
     * create-method. This will create new row in database according to supplied
     * valueObject contents. Make sure that values for all NOT NULL columns are
     * correctly specified. Also, if this table does not use automatic surrogate-keys
     * the primary-key must be specified. After INSERT command this method will 
     * read the generated primary-key back to valueObject if automatic surrogate-keys
     * were used. 
     *
     * @param conn         This method requires working database connection.
     * @param valueObject  This parameter contains the class instance to be created.
     *                     If automatic surrogate-keys are not used the Primary-key 
     *                     field must be set for this to work properly.
     */
    public synchronized void create(Connection conn, Funcionario valueObject) throws SQLException {

          String sql = "";
          PreparedStatement stmt = null;
          ResultSet result = null;

          try {
               sql = "INSERT INTO Funcionario ( id, nome, sobrenome, "
               + "RG, CPF, telefone, "
               + "senha, cargo, nivel, "
               + "salario) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
               stmt = conn.prepareStatement(sql);

               stmt.setInt(1, valueObject.getId()); 
               stmt.setString(2, valueObject.getNome()); 
               stmt.setString(3, valueObject.getSobrenome()); 
               stmt.setString(4, valueObject.getRG()); 
               stmt.setString(5, valueObject.getCPF()); 
               stmt.setString(6, valueObject.getTelefone()); 
               stmt.setString(7, valueObject.getSenha()); 
               stmt.setString(8, valueObject.getCargo()); 
               stmt.setInt(9, valueObject.getNivel()); 
               stmt.setObject(10, valueObject.getSalario(), Types.DOUBLE); 

               int rowcount = databaseUpdate(conn, stmt);
               if (rowcount != 1) {
                    //System.out.println("PrimaryKey Error when updating DB!");
                    throw new SQLException("PrimaryKey Error when updating DB!");
               }

          } finally {
              if (stmt != null)
                  stmt.close();
          }


    }


    /**
     * save-method. This method will save the current state of valueObject to database.
     * Save can not be used to create new instances in database, so upper layer must
     * make sure that the primary-key is correctly specified. Primary-key will indicate
     * which instance is going to be updated in database. If save can not find matching 
     * row, NotFoundException will be thrown.
     *
     * @param conn         This method requires working database connection.
     * @param valueObject  This parameter contains the class instance to be saved.
     *                     Primary-key field must be set for this to work properly.
     */
    public void save(Connection conn, Funcionario valueObject) 
          throws NotFoundException, SQLException {

          String sql = "UPDATE Funcionario SET nome = ?, sobrenome = ?, RG = ?, "
               + "CPF = ?, telefone = ?, senha = ?, "
               + "cargo = ?, nivel = ?, salario = ? WHERE (id = ? ) ";
          PreparedStatement stmt = null;

          try {
              stmt = conn.prepareStatement(sql);
              stmt.setString(1, valueObject.getNome()); 
              stmt.setString(2, valueObject.getSobrenome()); 
              stmt.setString(3, valueObject.getRG()); 
              stmt.setString(4, valueObject.getCPF()); 
              stmt.setString(5, valueObject.getTelefone()); 
              stmt.setString(6, valueObject.getSenha()); 
              stmt.setString(7, valueObject.getCargo()); 
              stmt.setInt(8, valueObject.getNivel()); 
              stmt.setObject(9, valueObject.getSalario(), Types.DOUBLE); 

              stmt.setInt(10, valueObject.getId()); 

              int rowcount = databaseUpdate(conn, stmt);
              if (rowcount == 0) {
                   //System.out.println("Object could not be saved! (PrimaryKey not found)");
                   throw new NotFoundException("Object could not be saved! (PrimaryKey not found)");
              }
              if (rowcount > 1) {
                   //System.out.println("PrimaryKey Error when updating DB! (Many objects were affected!)");
                   throw new SQLException("PrimaryKey Error when updating DB! (Many objects were affected!)");
              }
          } finally {
              if (stmt != null)
                  stmt.close();
          }
    }


    /**
     * delete-method. This method will remove the information from database as identified by
     * by primary-key in supplied valueObject. Once valueObject has been deleted it can not 
     * be restored by calling save. Restoring can only be done using create method but if 
     * database is using automatic surrogate-keys, the resulting object will have different 
     * primary-key than what it was in the deleted object. If delete can not find matching row,
     * NotFoundException will be thrown.
     *
     * @param conn         This method requires working database connection.
     * @param valueObject  This parameter contains the class instance to be deleted.
     *                     Primary-key field must be set for this to work properly.
     */
    public void delete(Connection conn, Funcionario valueObject) 
          throws NotFoundException, SQLException {

          String sql = "DELETE FROM Funcionario WHERE (id = ? ) ";
          PreparedStatement stmt = null;

          try {
              stmt = conn.prepareStatement(sql);
              stmt.setInt(1, valueObject.getId()); 

              int rowcount = databaseUpdate(conn, stmt);
              if (rowcount == 0) {
                   //System.out.println("Object could not be deleted (PrimaryKey not found)");
                   throw new NotFoundException("Object could not be deleted! (PrimaryKey not found)");
              }
              if (rowcount > 1) {
                   //System.out.println("PrimaryKey Error when updating DB! (Many objects were deleted!)");
                   throw new SQLException("PrimaryKey Error when updating DB! (Many objects were deleted!)");
              }
          } finally {
              if (stmt != null)
                  stmt.close();
          }
    }


    /**
     * deleteAll-method. This method will remove all information from the table that matches
     * this Dao and ValueObject couple. This should be the most efficient way to clear table.
     * Once deleteAll has been called, no valueObject that has been created before can be 
     * restored by calling save. Restoring can only be done using create method but if database 
     * is using automatic surrogate-keys, the resulting object will have different primary-key 
     * than what it was in the deleted object. (Note, the implementation of this method should
     * be different with different DB backends.)
     *
     * @param conn         This method requires working database connection.
     */
    public void deleteAll(Connection conn) throws SQLException {

          String sql = "DELETE FROM Funcionario";
          PreparedStatement stmt = null;

          try {
              stmt = conn.prepareStatement(sql);
              int rowcount = databaseUpdate(conn, stmt);
          } finally {
              if (stmt != null)
                  stmt.close();
          }
    }


    /**
     * coutAll-method. This method will return the number of all rows from table that matches
     * this Dao. The implementation will simply execute "select count(primarykey) from table".
     * If table is empty, the return value is 0. This method should be used before calling
     * loadAll, to make sure table has not too many rows.
     *
     * @param conn         This method requires working database connection.
     */
    public int countAll(Connection conn) throws SQLException {

          String sql = "SELECT count(*) FROM Funcionario";
          PreparedStatement stmt = null;
          ResultSet result = null;
          int allRows = 0;

          try {
              stmt = conn.prepareStatement(sql);
              result = stmt.executeQuery();

              if (result.next())
                  allRows = result.getInt(1);
          } finally {
              if (result != null)
                  result.close();
              if (stmt != null)
                  stmt.close();
          }
          return allRows;
    }


    /** 
     * searchMatching-Method. This method provides searching capability to 
     * get matching valueObjects from database. It works by searching all 
     * objects that match permanent instance variables of given object.
     * Upper layer should use this by setting some parameters in valueObject
     * and then  call searchMatching. The result will be 0-N objects in a List, 
     * all matching those criteria you specified. Those instance-variables that
     * have NULL values are excluded in search-criteria.
     *
     * @param conn         This method requires working database connection.
     * @param valueObject  This parameter contains the class instance where search will be based.
     *                     Primary-key field should not be set.
     */
    public List searchMatching(Connection conn, Funcionario valueObject) throws SQLException {

          List searchResults;

          boolean first = true;
          StringBuffer sql = new StringBuffer("SELECT * FROM Funcionario WHERE 1=1 ");

          if (valueObject.getId() != 0) {
              if (first) { first = false; }
              sql.append("AND id = ").append(valueObject.getId()).append(" ");
          }

          if (valueObject.getNome() != null) {
              if (first) { first = false; }
              sql.append("AND nome LIKE '").append(valueObject.getNome()).append("%' ");
          }

          if (valueObject.getSobrenome() != null) {
              if (first) { first = false; }
              sql.append("AND sobrenome LIKE '").append(valueObject.getSobrenome()).append("%' ");
          }

          if (valueObject.getRG() != null) {
              if (first) { first = false; }
              sql.append("AND RG LIKE '").append(valueObject.getRG()).append("%' ");
          }

          if (valueObject.getCPF() != null) {
              if (first) { first = false; }
              sql.append("AND CPF LIKE '").append(valueObject.getCPF()).append("%' ");
          }

          if (valueObject.getTelefone() != null) {
              if (first) { first = false; }
              sql.append("AND telefone LIKE '").append(valueObject.getTelefone()).append("%' ");
          }

          if (valueObject.getSenha() != null) {
              if (first) { first = false; }
              sql.append("AND senha LIKE '").append(valueObject.getSenha()).append("%' ");
          }

          if (valueObject.getCargo() != null) {
              if (first) { first = false; }
              sql.append("AND cargo LIKE '").append(valueObject.getCargo()).append("%' ");
          }

          if (valueObject.getNivel() != 0) {
              if (first) { first = false; }
              sql.append("AND nivel = ").append(valueObject.getNivel()).append(" ");
          }

          if (valueObject.getSalario() != null) {
              if (first) { first = false; }
              sql.append("AND salario = ").append(valueObject.getSalario()).append(" ");
          }


          sql.append("ORDER BY id ASC ");

          // Prevent accidential full table results.
          // Use loadAll if all rows must be returned.
          if (first)
               searchResults = new ArrayList();
          else
               searchResults = listQuery(conn, conn.prepareStatement(sql.toString()));

          return searchResults;
    }


    /** 
     * getDaogenVersion will return information about
     * generator which created these sources.
     */
    public String getDaogenVersion() {
        return "DaoGen version 2.4.1";
    }


    /**
     * databaseUpdate-method. This method is a helper method for internal use. It will execute
     * all database handling that will change the information in tables. SELECT queries will
     * not be executed here however. The return value indicates how many rows were affected.
     * This method will also make sure that if cache is used, it will reset when data changes.
     *
     * @param conn         This method requires working database connection.
     * @param stmt         This parameter contains the SQL statement to be excuted.
     */
    protected int databaseUpdate(Connection conn, PreparedStatement stmt) throws SQLException {

          int result = stmt.executeUpdate();

          return result;
    }



    /**
     * databaseQuery-method. This method is a helper method for internal use. It will execute
     * all database queries that will return only one row. The resultset will be converted
     * to valueObject. If no rows were found, NotFoundException will be thrown.
     *
     * @param conn         This method requires working database connection.
     * @param stmt         This parameter contains the SQL statement to be excuted.
     * @param valueObject  Class-instance where resulting data will be stored.
     */
    protected void singleQuery(Connection conn, PreparedStatement stmt, Funcionario valueObject) 
          throws NotFoundException, SQLException {

          ResultSet result = null;

          try {
              result = stmt.executeQuery();

              if (result.next()) {

                   valueObject.setId(result.getInt("id")); 
                   valueObject.setNome(result.getString("nome")); 
                   valueObject.setSobrenome(result.getString("sobrenome")); 
                   valueObject.setRG(result.getString("RG")); 
                   valueObject.setCPF(result.getString("CPF")); 
                   valueObject.setTelefone(result.getString("telefone")); 
                   valueObject.setSenha(result.getString("senha")); 
                   valueObject.setCargo(result.getString("cargo")); 
                   valueObject.setNivel(result.getInt("nivel")); 
                   valueObject.setSalario((Double)result.getObject("salario")); 

              } else {
                    //System.out.println("Funcionario Object Not Found!");
                    throw new NotFoundException("Funcionario Object Not Found!");
              }
          } finally {
              if (result != null)
                  result.close();
              if (stmt != null)
                  stmt.close();
          }
    }


    /**
     * databaseQuery-method. This method is a helper method for internal use. It will execute
     * all database queries that will return multiple rows. The resultset will be converted
     * to the List of valueObjects. If no rows were found, an empty List will be returned.
     *
     * @param conn         This method requires working database connection.
     * @param stmt         This parameter contains the SQL statement to be excuted.
     */
    protected List listQuery(Connection conn, PreparedStatement stmt) throws SQLException {

          ArrayList searchResults = new ArrayList();
          ResultSet result = null;

          try {
              result = stmt.executeQuery();

              while (result.next()) {
                   Funcionario temp = createValueObject();

                   temp.setId(result.getInt("id")); 
                   temp.setNome(result.getString("nome")); 
                   temp.setSobrenome(result.getString("sobrenome")); 
                   temp.setRG(result.getString("RG")); 
                   temp.setCPF(result.getString("CPF")); 
                   temp.setTelefone(result.getString("telefone")); 
                   temp.setSenha(result.getString("senha")); 
                   temp.setCargo(result.getString("cargo")); 
                   temp.setNivel(result.getInt("nivel")); 
                   temp.setSalario((Double)result.getObject("salario")); 

                   searchResults.add(temp);
              }

          } finally {
              if (result != null)
                  result.close();
              if (stmt != null)
                  stmt.close();
          }

          return (List)searchResults;
    }


}