/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hermanbeckyscompany.rhsys.model;

/**
 *
 * @author haruan
 */
import com.hermanbeckyscompany.rhsys.dao.ConnectionManager;
import com.hermanbeckyscompany.rhsys.dao.SistemaDao;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.math.*;

public class Sistema implements Cloneable, Serializable {

    private int id;
    private String nome;
    private Connection conn;

    public Sistema () {
        
    }

    public Sistema (int idIn) {

          this.id = idIn;

    }

    public static List getSistemaList(Sistema sistema) {
        Connection conn = ConnectionManager.getConnection();
        SistemaDao dao = new SistemaDao();
        List ret = new ArrayList();
        try {
            ret = dao.searchMatching(conn,sistema);
        } catch (Exception e) {
        }
        ConnectionManager.closeAll(conn);
        return ret;
    }
    
    public static List getSistemaList() {
        Connection conn = ConnectionManager.getConnection();
        SistemaDao dao = new SistemaDao();
        List ret = new ArrayList();
        try {
            ret = dao.loadAll(conn);
        } catch (Exception e) {
        }
        ConnectionManager.closeAll(conn);
        return ret;
    }
    
    public void delete() {
        Connection conn = ConnectionManager.getConnection();
        SistemaDao dao = new SistemaDao();
        try {
            dao.delete(conn,this);
        } catch (Exception e) {
        }
        ConnectionManager.closeAll(conn);
    }
    
    /** 
     * Get- and Set-methods for persistent variables. The default
     * behaviour does not make any checks against malformed data,
     * so these might require some manual additions.
     */

    public int getId() {
          return this.id;
    }
    public void setId(int idIn) {
          this.id = idIn;
    }

    public String getNome() {
          return this.nome;
    }
    public void setNome(String nomeIn) {
          this.nome = nomeIn;
    }



    /** 
     * setAll allows to set all persistent variables in one method call.
     * This is useful, when all data is available and it is needed to 
     * set the initial state of this object. Note that this method will
     * directly modify instance variales, without going trough the 
     * individual set-methods.
     */

    public void setAll(int idIn,
          String nomeIn) {
          this.id = idIn;
          this.nome = nomeIn;
    }


    /** 
     * hasEqualMapping-method will compare two Sistema instances
     * and return true if they contain same values in all persistent instance 
     * variables. If hasEqualMapping returns true, it does not mean the objects
     * are the same instance. However it does mean that in that moment, they 
     * are mapped to the same row in database.
     */
    public boolean hasEqualMapping(Sistema valueObject) {

          if (valueObject.getId() != this.id) {
                    return(false);
          }
          if (this.nome == null) {
                    if (valueObject.getNome() != null)
                           return(false);
          } else if (!this.nome.equals(valueObject.getNome())) {
                    return(false);
          }

          return true;
    }

    public void save() {
        Connection conn = ConnectionManager.getConnection();
        SistemaDao dao = new SistemaDao();
        try {
            if (this.id == 0) {
                System.out.println("Cria Sistema");
                dao.create(conn, this);
            } else {
                dao.save(conn, this);
            }
        } catch (Exception e) {
        }
        ConnectionManager.closeAll(conn);
    }
    
    
    /**
     * toString will return String object representing the state of this 
     * valueObject. This is useful during application development, and 
     * possibly when application is writing object states in textlog.
     */
    public String toString() {
        StringBuffer out = new StringBuffer();
        out.append("id = " + this.id + "\n"); 
        out.append("nome = " + this.nome + "\n"); 
        return out.toString();
    }


    /**
     * Clone will return identical deep copy of this valueObject.
     * Note, that this method is different than the clone() which
     * is defined in java.lang.Object. Here, the retuned cloned object
     * will also have all its attributes cloned.
     */
    public Object clone() {
        Sistema cloned = new Sistema();

        cloned.setId(this.id); 
        if (this.nome != null)
             cloned.setNome(new String(this.nome)); 
        return cloned;
    }



    /** 
     * getDaogenVersion will return information about
     * generator which created these sources.
     */
    public String getDaogenVersion() {
        return "DaoGen version 2.4.1";
    }

}